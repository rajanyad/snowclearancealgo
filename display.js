var markers = [];
var map;
function initMap() {
    
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: new google.maps.LatLng(40.4824783, -74.4303398),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
}

delay = 40
function startJourney() {

    var route = [[40.4824783, -74.4303398], [40.4807483, -74.4285838], [40.4802384, -74.4282541], [40.477936, -74.4277809]]

    var startPos, endPos;
    size = route.length - 1
    for(var pos = 0; pos < size; pos++) {
        startPos = new google.maps.LatLng(route[pos][0], route[pos][1])
        endPos = new google.maps.LatLng(route[pos+1][0], route[pos+1][1])


//    var startPos = new google.maps.LatLng(40.4824783, -74.4303398);
//    var endPos = new google.maps.LatLng(40.482834, -74.4285886);

        // generate fake points for route
        var pointsNo = 50;
        var latDelta = (endPos.lat() - startPos.lat()) / pointsNo;
        var lngDelta = (endPos.lng() - startPos.lng()) / pointsNo;
        var positions = [];
        for (var i = 0; i < pointsNo; i++) {
            var curLat = startPos.lat() + i * latDelta;
            var curLng = startPos.lng() + i * lngDelta;
            positions.push(new google.maps.LatLng(curLat, curLng));

            var curMarker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(curLat, curLng),
                visible: false,
                icon: "./car.png"
            });
            markers.push(curMarker);
        }
        console.log("HERE!")
        displayMarker(markers, 0)
    }
}

function displayMarker(markers, index) {
    console.log(index)
    if (index > 0)
        markers[index - 1].setVisible(false);
    else {
        markers[markers.length - 1].setVisible(false);
    }

    markers[index].setVisible(true);
    if (index < markers.length - 1) {
        setTimeout(function () {
            displayMarker(markers, index + 1);
        }, delay);
    }
}

